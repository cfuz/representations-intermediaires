Etudiants | Date | Sujet
:---|:---|:---
Jonathan Heno | 09/05/2020 | TP2 - Analyse d'embeddings 

# Compte rendu
## Introduction
L'unité complémentaire d'enseignement **Application IA**, a pour objectif de nous présenter les rudiments des réseaux de neurones.
Dans le cadre des présents travaux, nous souhaitons analyser les sorties des couches cachées d'un réseau de neurones profond, visant à transcrire les mots prononcés et détecter des concepts sémantiques, dans le contexte d'une application informatique de *réservation d'hôtel par dialogue téléphonique humain machine*.

Après avoir visualisé les embeddings de chacune des couches cachées calculées sur le corpus de validation, nous construirons un perceptron multicouche (ou MLP) devant déduire le concept sémantique, à partir des informations de sortie de la couche précédente.
Nous comparerons alors les performances du système ainsi construit sur chacune des couches intermédiaires.
L'on s'attend notamment à ce que le temps de résolution diminue avec la distance de la couche cachée à celle de sortie.


## Visualisation des embeddings
### Conversion des fichiers du jeu de validation au format `.tsv`
En utilisant la commande Unix Stream EDitor (`sed`) il nous suffit de remplacer les virgules composant les données des fichiers `dev.embed.layer-*`, par des tabulations.
Le script `src/to-tsv.sh` permet de réaliser la conversion au format `.tsv` des fichiers `dev.embed.layer-*`, en un fichier de données contenant les embeddings (`data/tsv/vec-dev.embed.layer-*.tsv`) et un fichier de métadonnées les qualifiant (`data/tsv/metadata-dev.embed.layer-*.tsv`).

### Chargement et visualisation des fichiers sur Tensorflow
Via le [module de projection des données](http://projector.tensorflow.org) de la plateforme [Tensorflow](https://www.tensorflow.org), nous pouvons observer les représentations qui suivent :

#### Couche 1
![umap-layer-1](img/umap-layer-1.png)

#### Couche 2
![umap-layer-2](img/umap-layer-2.png)

#### Couche 3
![umap-layer-3](img/umap-layer-3.png)


#### Couche 4
![umap-layer-4](img/umap-layer-4.png)

#### Couche 5
![umap-layer-5](img/umap-layer-5.png)

## Interprétation
L'on remarque que dès la troisième couche cachée, les embeddings décrivent des clusters de plus en plus précis. 
Au fil des couches, les représentations se précisent et permettent au réseau de neurone de mieux classifier des données qui lui sont fournies, ce qui semble à première vue corroborer l'hypothèse formulée en introduction.

## Analyse des embeddings via un MLP
### Avant-propos
#### Pré-requis
Votre environnement python doit inclure les bibliothèques `pytorch` et `numpy`.

#### Données
Les données d'exploitation sont à télécharger à part et à placer dans un dossier `data/`, comme ci-dessous :

![data-dir](img/data-dir.png)


#### Utilisation
L'application s'utilise comme suit :
```
python src/net.py [-h] -li {1,2,3,4,5} [-lr LEARNING_RATE] [-m MOMENTUM] [-e EPOCHS] [-hd HIDDEN_DIM] [-bs BATCH_SIZE] [-p PATIENCE] [-v] [-cl]

optional arguments:
  -h, --help            show this help message and exit
  -li {1,2,3,4,5}, --layer_id {1,2,3,4,5}
                        ID of the dataset to process (contained in the data/ directory)
  -lr LEARNING_RATE, --learning_rate LEARNING_RATE
                        Learning rate of each unit composing the model (default: 0.01)
  -m MOMENTUM, --momentum MOMENTUM
                        Model's momentum (default: 0.9)
  -e EPOCHS, --epochs EPOCHS
                        Maximum number of iterations used for the learning process (default: 20)
  -hd HIDDEN_DIM, --hidden_dim HIDDEN_DIM
                        Number of logic units per hidden layer (default: 64)
  -bs BATCH_SIZE, --batch_size BATCH_SIZE
                        Number of processed sequences per iteration (default: 128)
  -p PATIENCE, --patience PATIENCE
                        Number of epochs to wait before stopping the learning process (default: 7)
  -v, --verbose         Starts the script in explicit mode (default: false)
  -cl, --clear_logs     Clear the logs linked to `dataset_id` (default: false)
```
Pensez surtout à lancer le script `net.py` depuis la racine du projet. Depuis cette localisation, à titre d'exemple, si vous souhaitez générer un modèle en l'entraînant sur les embeddings de la couche 1, pour un maximum de 100 itérations :
```bash
python src/net.py -li 1 -e 100 -v
```

### Arborescence des fichiers du projet
Ayant fait l'effort de commenter entièrement mon code, je ne m'étendrai pas sur les détails d'implémentation.
#### Les sources (`src/`)
##### `parser.py`
Permet de transcrire les arguments fournis par l'utilisateur au lancement de l'application.
Par ailleurs ce module dispose d'une fonction de parsing préliminaire des fichiers de données correspondant aux embeddings d'une couche de profondeur donnée.
##### `dataset.py`
Elle contient une structure spécialisée de la classe `data.Dataset` afin de gérer les données provenant du module de parsing.
##### `early_stopper.py`
Contient la définition de ma classe en charge de la gestion de l'arrêt prématuré de la phase d'entraînement du modèle.
##### `net.py`
Point d'orgue de l'application mais également point d'entrée, ce script définit le modèle du perceptron multicouche.
Il coordonne et utilise, par ailleurs, l'ensemble des structures énoncées plus haut.
##### `benchmark.py`
Son rôle est simplement d'évaluer à multiples reprises, les performances du modèle sur les différents embeddings fournis dans le cadre de ces travaux.
Il permet d'automatiser la phase de génération des données statistiques de sortie et, au besoin, de faire varier les différents paramètres définissant le modèle en vue de les optimiser.

#### Le dossier des sorties (`out/`)
Il contient tous les fichiers de sortie générés par l'application.

#### Le dossier contenant les sauvegardes du modèle (`cpt/`)
Tout est dans le titre.

### Condition d'expérimentation
#### Paramètres du modèle
Dans le cadre des mesures prises pour les réalisations graphiques ci-dessous, le modèle a été lancé selon les modalités suivantes :
- `criterion` : `CrossEntropyLoss`
- `optimizer` : `SGD` avec taux d'apprentissage fixé à **0.01** et inertie à **0.9**
- Nombre d'unité de la couche cachée : **64**
- Nombre d'entrées par mini-batch: **128**
- Patience : **3**
- Nombre d'itérations maximum : **100**

#### Statistiques
Ayant remarqué une fluctuation somme toute assez importante dans les résultats en sortie, j'ai décidé de lancé mon modèle 50 fois sur chacun des embeddings, afin de pouvoir en analyser la distribution et les tendances centrales.
De surcroît, j'ai filtré les résultats pour lesquels la valeur d'un indicateur était aberrante (*i.e.* dont la valeur était en-dehors de l'intervalle *moyenne ±écart-type*).

### Performances relatives en fonction de la profondeur de la couche cachée
Tous les graphiques affichés ci-dessous disposent d'une **version HTML interactive** (et accessoirement, plus lisible) dans le dossier `html/`.
J'ai par ailleurs décidé de combiner pour chaque type d'indicateur, une représentation couplant, pour chaque niveau du réseau profond :
- une répartition des différentes métriques sous forme densitaire (*violons et nuage de points*)
- l'étendue des valeurs, leurs médianes et moyennes (*boîtes à moustaches*)

#### Précision
![accuracy](img/accuracy.png)

#### Minimum de la fonction de coût
![min-loss](img/min-loss.png)

#### Nombre d'itération avant convergence
![epoch](img/epoch.png)

## Conclusion
### Analyse des résultats
D'après les deux premiers graphiques, on dénote une anti-corrélation claire entre la précision du modèle et le minimum de la fonction de coût, tant sur le plan des distributions que des moyennes.
Si l'on considère l'évolution de la moyenne de précision du modèle, en fonction de la profondeur de la couche qui lui est donné d'analyser : plus la couche est superficielle, plus la précision est importante.
Cependant, passé la quatrième couche, cette tendance tend à s'annuler, ce qui nous laisse donc penser que la cinquième couche du réseau profond n'était pas spécialement nécessaire.
L'on prendra soin de mettre en relief les fluctuations importantes du modèle sur les métriques `accuracy` et `min_loss`. 
Ceci peut-être dû au choix du `criterion` en charge de rétropropager les erreurs d'estimation du modèle.
Il se pourrait également que les conditions d'arrêt prématuré soient trop brutales pour permettre au système de trouver une configuration stable.
Peut-être aurait-il fallu évaluer avec un facteur de `patience` plus important au détriment du temps de résolution.

Aussi, concernant le dernier graphique, on remarquera que le nombre d'itérations moyen, nécessaire à l'analyse des embeddings, présente un minima pour les sorties de la deuxième couche du réseau profond, avant de revenir à une moyenne comparable à celle de la première couche.
De plus, les fluctuations de cet indicateur tendent à grandement croître avec la superficialité de la couche.
Tout cela laisse supposer que si l'on souhaite trouver un équilibre entre temps de résolution et précision, il suffirait de redéfinir le réseau profond sur trois couches cachées.

### Points d'amélioration
Je n'ai actuellement toujours pas développé mon modèle autour de l'utilisation de processeurs graphiques.
Cependant les données restent acceptables en terme de volume pour pouvoir être traitées par un CPU moderne, en des temps raisonnables. 
Je pense cela dit essayé d'intégrer cette composante dans mes futurs projets.
Par ailleurs, la phase de parsing des fichiers d'entrée reste une des étapes les plus longues du processus. 
Il faudrait que je me penche plus avant, sur des solutions permettant de paralléliser ces traitements, soit via la bibliothèque `pytorch` soit via une bibliothèque tierce.