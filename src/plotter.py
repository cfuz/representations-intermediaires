import parser
import numpy
import plotly.graph_objects as go



def plot_benchmarks():
    data = {}

    for layer_id in range(1, 6):
        data[layer_id] = parser.parse_output(layer_id)
    
    to_html(data, 'accuracy')
    to_html(data, 'min_loss')
    to_html(data, 'epoch')

def to_html(data, dtype):
    dtype_formatted = dtype.replace('_', '. ')
    dtype_formatted = dtype_formatted.title()

    fig = go.Figure({
        'data': [
            go.Violin({
                'x0': f'Layer {layer_id}',
                'y': data[layer_id][dtype],
                'name': f'layer-{layer_id}',
                'box_visible': True,
                'meanline_visible': True,
                'points': 'all', # show all points
                'jitter': 0.05,  # add some jitter on points for better visibility
                'scalemode': 'count'
            }) for layer_id in data
        ]
    })

    fig.update_layout({
        'title': {
            'text': f'{dtype_formatted} distribution given layer depth',
            'y':0.95,
            'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top',
            'font': {
                'family': 'Ubuntu, bold',
                'size': 20,
            }
        },
        'xaxis': {
            'showgrid': False,
            'zeroline': False,
        },
        'yaxis': {
            'title': f'{dtype_formatted} {"(%)" if dtype == "accuracy" else ""} distribution',
            'zeroline': False,
            'gridcolor': 'white',
        },
        'font': {
            'family': 'Ubuntu',
        },
        'showlegend': False,
        'paper_bgcolor': 'rgb(233,233,233)',
        'plot_bgcolor': 'rgb(233,233,233)',
    })
    fig.write_html(f'html/{dtype}.html')


if __name__ == "__main__":
    plot_benchmarks()