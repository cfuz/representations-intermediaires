#!/usr/bin/env python3.8
# coding: utf-8
from __future__ import print_function
from torch import nn
from early_stopper import EarlyStopper
from parser import *
from logger import Logger
from dataset import *
import sys
import torch



class Net(nn.Module):
    '''
    Classe contenant la structure du modèle pytorch pour l'analyse des 
    séquences de protéines.

    Attributs
    ----------
    lr: float
        Pas d'apprentissage
    momentum: int
        Momentum du modèle
    input_size: int
        Nombre de features caractérisant les entrées
    output_size: int
        Dimension des données de sortie (trois en l'occurence ici puisque le 
        jeu de données ne présente que trois structures secondaires différentes)
    hidden_dim: int
        Nombre de neurones composants la couche cachée
    batch_size: int
        Taille des mini-batchs
    '''

    def __init__(
        self,
        lr,
        momentum,
        input_size,
        output_size,
        hidden_dim,
        batch_size
    ):
        super( Net, self ).__init__()

        self.lr             = lr
        self.momentum       = momentum
        self.input_size     = input_size
        self.output_size    = output_size
        self.hidden_dim     = hidden_dim
        self.batch_size     = batch_size

        # Liaison entrées --> couche cachée
        self.fc1 = nn.Linear( self.input_size, self.hidden_dim )
        self.relu1 = nn.ReLU()
        # Liaison couche cachée --> sorties
        self.fc2 = nn.Linear( self.hidden_dim, self.output_size )
        self.relu2 = nn.ReLU()



    def forward(self, x):
        '''
        Propagation des caractéristiques de l'individu x dans le réseau 
        (i.e. définition du système de feed-forward).

        Paramètre
        ----------
        x: torch.tensor
            Entrée de taille `self.input_size` composant le jeu d'entraînement,
            à propager dans le réseau.
        
        Retourne
        ----------
        torch.tensor
            Sortie filtrée selon la fonction de rectification linéaire 
            appliquée au vecteur de sortie (ici de dimension 3).
        '''
        x = self.fc1( x )
        x = self.relu1( x )
        x = self.fc2( x )
        return self.relu2( x )



    def fire(
        self, 
        train_dataloader, 
        validation_dataloader, 
        epochs,
        patience,
    ):
        '''
        Lance la procédure d'apprentissage du réseau.

        Paramètres
        ----------
        train_dataloader: torch.data.DataLoader
            Liste des protéines composant le jeu d'entraînement
        epochs: int
            Nombre maximum d'itération dans le processus d'apprentissage
        '''
        Logger.info('Learning process fired!')

        epoch = 0

        # Filtrage des résultats avec un softmax en sortie de modèle
        criterion = nn.CrossEntropyLoss()
        optimizer = torch.optim.SGD(
            net.parameters(), 
            lr = self.lr, # valeur conseillée: 0.01???
            momentum = self.momentum
        )
        '''
        optimizer = torch.optim.Adam(
            net.parameters(), 
            lr = self.lr # valeur conseillée: 0.001???
        )
        '''

        # Instanciation de la classe en charge de la gestion de l'arrêt 
        # prématuré de l'entraînement du modèle.
        early_stopper = EarlyStopper( patience, delta = 1.0e-8 )

        while epoch < epochs:
            '''
            Phase d'entraînement du modèle
            '''
            # Configure le modèle pour l'entraînement
            self.train()
            avg_training_loss, running_loss = 0.0, 0.0
            for batch_id, (inputs, labels) in enumerate( train_dataloader ):
                # Vide les gradients de toutes les variables optimisées
                optimizer.zero_grad()
                # Génération des prédictions par propagation avant des données 
                # du jeu.
                estimated_output = self(inputs)
                # Calcul du taux d'erreur lié aux prédictions ainsi réalisées 
                # par le modèle.
                loss = criterion(estimated_output, labels)
                # Rétropropagation des erreurs de prédicitions en regard du 
                # criterion définit plus haut.
                loss.backward()
                # On réalise une étape d'optimisation pour mettre à jour les 
                # paramètres du réseau. 
                optimizer.step()
                # Logs périodiques d'avancement et réinitialisation du compteur 
                # si programme lancé en mode verbeux.
                if Logger.verbose:
                    # On trace la loss pour calculer la moyenne des valeurs 
                    # renvoyées par cette fonction sur `periodic_checks` mini-batchs.
                    running_loss += loss.item()
                    avg_training_loss += loss.item()
                    if batch_id % 50 == 49:
                        running_loss_ratio = float(running_loss / 100)
                        Logger.update(f'{"Learning status..":<28} [ epoch: {epoch:>3}, batch id: {batch_id:>4}, avg. loss: {running_loss_ratio:>9.5f} ]')
                        running_loss = 0.0
            if Logger.verbose:
                avg_training_loss /= len( train_dataloader.dataset )
                Logger.update(f'{f"Avg. training loss: {avg_training_loss:.3f}":<28} [ epoch: {epoch:>3}, batch id: {batch_id:>4}, avg. loss: {running_loss_ratio:>9.5f} ]')
            
            '''
            Phase d'évaluation
            '''
            # Configure le réseau pour évaluation de ses paramètres
            self.eval()
            avg_validation_loss = 0.0
            accuracy = 0.0
            with torch.no_grad():
                for data, label in validation_dataloader:
                    output = self(data)
                    prediction = output.argmax( dim = 1, keepdim = True )
                    loss = criterion(output, label)
                    avg_validation_loss += loss.item()
                    accuracy += prediction.eq( label.view_as(prediction) ).sum().item()
            avg_validation_loss /= len( validation_dataloader.dataset )
            accuracy /= len( validation_dataloader.dataset )

            '''
            Vérification de la valeur de la moyenne de la fonction de coût sur 
            le jeu de validation. Si cette quantité a diminué, on créé une 
            sauvegarde/checkpoint de la configuration du modèle.
            '''
            early_stopper(avg_validation_loss, self, epoch, accuracy)
            if early_stopper.early_stop:
                Logger.update('Early stopping!')
                break

            epoch += 1
        
        '''
        Sorti de la boucle d'apprentissage on charge la configuration 
        minimisant la fonction de coût sur le jeu de validation via le 
        checkpoint stocké par l'`early_stopper`.
        '''
        self.load_state_dict(torch.load(f'cpt/checkpoint-{Logger.layer_id}.pt'))
        Logger.info(f'{"Status":<28} [ epochs: {epoch:>3}, acc.: {early_stopper.accuracy * 100.0:>7.3f}, min. loss: {early_stopper.min_loss:>9.5f} ]')
        Logger.subinfo(f'Logging performances..')
        Logger.log_accuracy(epoch, early_stopper.accuracy, early_stopper.min_loss)



    def __str__(self):
       return 'Net\n   {:<13}: {}\n   {:<13}: {}\n   {:<13}: {}\n   {:<13}: {}\n   {:<13}: {}\n'.format(
            'lr', 
            self.lr,
            'momentum', 
            self.momentum,
            'input_size',
            self.input_size,
            "hidden_dim",
            self.hidden_dim,
            'output_size',
            self.output_size,
        )



    def __repr__(self):
        return 'Net {{ "lr": {:.3f}, "momentum": {}, "input_size": {}, "hidden_dim": {}, "output_size": {} }}'.format(
            self.lr,
            self.momentum,
            self.input_size,
            self.hidden_dim,
            self.output_size
        )




if __name__ == "__main__":
    '''
    Exemple d'utilisation du modèle
    ----------
        python3 src/net.py -i 1 -v
    '''
    args = parse_args()

    # On active le Logger si nécessaire
    Logger.verbose = args.verbose
    Logger.layer_id = args.layer_id

    # Vide le fichier de log si nécessaire
    if args.clear_logs:
        Logger.clear_logs()

    # Définition des paramètres du mini-batch
    minibatch_setup = {
        'batch_size': args.batch_size,
        'shuffle': True,
        'num_workers': 4
    }

    # Création des DataLoader pour l'entraînement du modèle
    raw_datasets = to_datasets( args.layer_id )
    train_dataloader = torch.utils.data.DataLoader( 
        Dataset(
            raw_datasets['train']['inputs'], 
            raw_datasets['train']['concepts'], 
        ), 
        **minibatch_setup 
    )
    # On récupère le dictionnaire de concepts constitués lors de la phase 
    # d'instanciation du Dataset.
    concept_bundle = train_dataloader.dataset.bundle

    test_dataloader = torch.utils.data.DataLoader( 
        Dataset(
            raw_datasets['test']['inputs'], 
            raw_datasets['test']['concepts'],
            concept_bundle
        ), 
        **minibatch_setup 
    )
    validation_dataloader = torch.utils.data.DataLoader( 
        Dataset(
            raw_datasets['validation']['inputs'], 
            raw_datasets['validation']['concepts'],
            concept_bundle
        ), 
        **minibatch_setup 
    )

    # Récupération du nombre d'itérations maximum du processus d'apprentissage
    epochs = args.epochs
    input_size = list( train_dataloader.dataset.inputs.size() )[1]
    output_size = train_dataloader.dataset.bundle['size']
    
    # Création du modèle
    net = Net(
        args.learning_rate,
        args.momentum,
        input_size,
        output_size,
        args.hidden_dim,
        args.batch_size
    )
    Logger.info( 'Network configuration:\n{}'.format( net ) )
    Logger.empty_line()

    # Lancement de la procédure d'entraînement
    net.fire(
        train_dataloader,
        validation_dataloader,
        epochs,
        args.patience,
    )

    Logger.empty_line()
    Logger.success( 'Done!' )
