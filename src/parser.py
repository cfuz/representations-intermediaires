#!/usr/bin/env python3.8
# coding: utf-8
import numpy as np
import argparse
import sys
import torch

from dataset import Dataset
from logger import Logger



# Définition des constantes du modèle
EPOCHS          = 20
LR              = .01
MOMENTUM        = .9
HIDDEN_DIM      = 64
BATCH_SIZE      = 128
PATIENCE        = 3



def parse_args():
    '''
    Parse les arguments entrés par l'utilisateur au lancement du programme

    Returns
    ----------
    Namespace(
        batch_size: int, 
        layer_id: int, 
        epochs: int, 
        hidden_dim: int, 
        learning_rate: float, 
        momentum: float, 
        patience: int,
        verbose: bool,
        clear_logs: bool
    )
        Le namespace correspondant aux arguments parsés
    '''
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-li", "--layer_id", 
        required = True,
        type = int,
        choices = [1, 2, 3, 4, 5],
        help = 'ID of the dataset to process (contained in the data/ directory)'
    )
    parser.add_argument(
        '-lr', '--learning_rate',
        type = float,
        default = LR,
        help = f'Learning rate of each unit composing the model (default: {LR})'
    )
    parser.add_argument(
        '-m', '--momentum',
        type = float,
        default = MOMENTUM,
        help = f'Model\'s momentum (default: {MOMENTUM})'
    )
    parser.add_argument(
        '-e', '--epochs',
        type = int,
        default = EPOCHS,
        help = f'Maximum number of iterations used for the learning process (default: {EPOCHS})'
    )
    parser.add_argument(
        '-hd', '--hidden_dim',
        type = int,
        default = HIDDEN_DIM,
        help = f'Number of logic units per hidden layer (default: {HIDDEN_DIM})'
    )
    parser.add_argument(
        '-bs', '--batch_size',
        type = int,
        default = BATCH_SIZE,
        help = f'Number of processed sequences per iteration (default: {BATCH_SIZE})'
    )
    parser.add_argument(
        '-p', '--patience',
        type = int,
        default = PATIENCE,
        help = f'Number of epochs to wait before stopping the learning process (default: {PATIENCE})'
    )
    parser.add_argument(
        '-v', '--verbose',
        action = 'store_true',
        help = 'Starts the script in explicit mode (default: false)'    
    )
    parser.add_argument(
        '-cl', '--clear_logs',
        action='store_true',
        help = 'Clear the logs linked to `dataset_id` (default: false)'
    )
    
    return parser.parse_args()


def to_datasets(dataset_id):
    '''
    Parses datasets from a given ID to process in data/ directory

    Parameters
    ----------
    dataset_id: int
        ID of the dataset

    Returns
    ----------
    datasets: HasMap( str, ( Dataset, list(str)) )
        Map containing the dataset with train, test and validation datasets.
        The second element in every tuple informs about the uniq meaning values in the dataset.

    Nota
    ----------
    Each Dataset in output contains tensors as inputs and targets
    '''
    filenames = [ 'data/train.embed.layer-', 'data/test.embed.layer-', 'data/dev.embed.layer-' ]
    dataset_names = [ 'train', 'test', 'validation' ]

    datasets = {  }

    for index, filename in enumerate(filenames):
        datasets[dataset_names[index]] = parse_file(filename + str(dataset_id))

    return datasets


def parse_file(filename):
    file = open(filename, 'r')

    raw_dataset = { 'inputs': [], 'concepts': [] }

    for line in file:
        buffer = line.split('\n')[0].split(',')
        raw_dataset['inputs'] += [ [ float(weight) for weight in buffer[2].split() ] ]
        raw_dataset['concepts'] += [ buffer[1] ]

    file.close()

    return raw_dataset


def parse_output(layer_id):
    '''
    Récupère les données d'un fichier de sortie liées à un embedding (référencé
    selon son `layer_id`)

    Paramètres
    ----------
    layer_id: int
        Indice de la série de données de sortie à traiter
    
    Retourne
    ----------
    dataset: { 'epoch': list(int), 'accuracy': list(float), 'min_loss': list(float) }
        Dictionnaire des données formattées
    '''
    file = open(f'out/bench-embed-layer-{layer_id}', 'r')
    first_line = True

    dataset = { 'epoch': [], 'accuracy': [], 'min_loss': [] }

    for line in file:
        if first_line: 
            first_line = False
        else:
            buffer = line.split('\n')[0].split('\t')
            dataset['epoch'] += [ int(buffer[0]) ]
            dataset['accuracy'] += [ float(buffer[1]) ]
            dataset['min_loss'] += [ float(buffer[2]) ]

    file.close()

    return dataset


if __name__ == "__main__":
    args = parse_args()
    Logger.verbose = args.verbose
    raw_datasets = to_datasets(args.dataset_id)
    for key in raw_datasets:
        print(
            '--> {:<10}: inputs_shape: {}, concepts_shape: {}'.format(
                key,
                '{:>6} x {:>6}'.format( 
                    len(raw_datasets[key]['inputs']), 
                    len(raw_datasets[key]['inputs'][0]) 
                ),
                len(raw_datasets[key]['concepts'])
            )
        )
