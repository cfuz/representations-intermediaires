#!/usr/bin/env python3.8
# coding: utf-8
import os



EPOCHS = 100
PATIENCE = 5



print('Benchmarking model..\n')
for layer_id in range(1, 6):
    print(f'※   layer id: {layer_id}') 
    for run_id in range(50):
        print(f'   ›  run: {run_id}')
        os.system(f'python3 src/net.py -li {layer_id} -e {EPOCHS} -p {PATIENCE} {"-cl" if run_id == 0 else ""}')
