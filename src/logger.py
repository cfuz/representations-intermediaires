#!/usr/bin/env python3.8
# coding: utf-8



class Logger:
    verbose = False
    layer_id = None

    def __repr__(self):
        return 'Logger {{ @static "verbose": {} }}'.format(
            Logger.verbose,
        )

    @staticmethod
    def empty_line():
        if Logger.verbose:
            print()

    @staticmethod
    def info(message):
        if Logger.verbose:
            print(f"➜  {message}")
    
    @staticmethod
    def subinfo(message):
        if Logger.verbose:
            print(f"   {message}")

    @staticmethod
    def subsubinfo(message):
        if Logger.verbose:
            print(f"      {message}")

    @staticmethod
    def success(message):
        if Logger.verbose:
            print(f"✔  {message}")
    
    @staticmethod
    def update(message):
        if Logger.verbose:
            print(f"ϟ  {message}")
    
    @staticmethod
    def warning(message):
        if Logger.verbose:
            print(f"☢  {message}")
    
    @staticmethod
    def error(message):
        if Logger.verbose:
            print(f"✘  {message}")

    @staticmethod
    def log_accuracy(epochs, accuracy, loss):
        file = open( f'out/bench-embed-layer-{Logger.layer_id}', 'a+' )
        file.write( f'{epochs:<6}\t{accuracy * 100.0:<9.5f}\t{loss:.5f}\n' )
        file.close()
       
    @staticmethod
    def clear_logs():
        file = open( f'out/bench-embed-layer-{Logger.layer_id}', 'w' )
        file.write(f'{"epochs":<6}\t{"accuracy":<9}\tmin. loss\n')
        file.close()
