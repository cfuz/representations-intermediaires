#!/usr/bin/env python3.8
# coding: utf-8
import torch
from torch.utils import data



class Dataset(data.Dataset):
    '''
    Structure définissant le jeu de données pour PyTorch. 
    Pour son utilisation dans le cadre de l'entraînement d'un modèle :
    https://stanford.edu/~shervine/blog/pytorch-how-to-generate-data-parallel

    Attributs
    ----------
    inputs: torch.tensor( float )
        Matrice de flottants composants les embeddings
    labels: list( int )
        Liste des concepts associés à chaque embedding
    bundle: { 
        'to_id': dict( str: int ), 
        'to_concept': dict( int: str ), 
        'size': int 
    }
        Dictionnaire de dictionnaires permettant de faire la translation 
        d'un concept en son équivalent entier ('to_id') et inversement 
        ('to_concept'). 
        'size' renseigne également le nombre de mots cconcepts uniques 
        composant le vocabulaire du jeu de données.
    '''
    def __init__(
        self, 
        inputs, 
        labels, 
        bundle = None,
    ):
        '''
        Initialisation du jeu de données selon les protéines et la liste des
        structures secondaires (étiquettes) composant le jeu de données.

        Paramètres
        ----------
        inputs: list( list( float ) )
            Matrice des entrées.
        labels: list( str )
            Liste des labels associés à chaque entrée du jeu de données
        bundle: { 
            'to_id': dict( str: int ), 
            'to_concept': dict( int: str ), 
            'size': int 
        } || None
            Dictionnaire de dictionnaires permettant de faire la translation 
            d'un concept en son équivalent entier ('to_id') et inversement 
            ('to_concept'). 
            'size' renseigne également le nombre de mots cconcepts 
            uniques omposant le vocabulaire du jeu de données.
        '''
        self.inputs = torch.tensor( inputs, dtype = torch.float )
        if bundle is None:
            self.bundle = Dataset.bundle_from(labels)
        else:
            self.bundle = bundle
        self.labels = torch.tensor(
            [ self.bundle['to_id'][label] for label in labels ],
            dtype = int
        )


    @staticmethod
    def bundle_from(labels):
        '''
        Créé un dictionnaire à partir d'une liste d'étiquettes entrées sous 
        forme de strings ainsi que le tenseur d'entiers correspondant à cette 
        liste.

        Paramètres
        ----------
        labels: list( str )
            Liste des étiquettes associées à un jeu de données

        Retourne
        ----------
        { 'to_id': dict( str: int ), 'to_concept': dict( int: str ), 'size': int }
            Dictionnaire de dictionnaires permettant de faire la translation 
            d'un concept en son équivalent entier ('to_id') et inversement 
            ('to_concept'). 
            'size' renseigne également le nombre de mots cconcepts 
            uniques omposant le vocabulaire du jeu de données.
        '''
        labels_set = sorted( set(labels) )
        to_id = { }
        to_concept = { }

        for index, concept in enumerate(labels_set):
            to_id[concept] = index
            to_concept[index] = concept
        
        return {
            'to_id': to_id, 
            'to_concept': to_concept, 
            'size': len( labels_set ) 
        }



    def __len__( self ):
        '''
        Fonction intrinsèque à la classe retournant le nombre total d'entrées la composant

        Retourne
        ----------
        int
            Nombre d'individus composant le jeu de données
        '''
        return len( self.labels )
    

    def __getitem__( self, index ):
        '''
        Récupère un élèment du jeu de données selon son indice

        Paramètre
        ----------
        index: int
            Indice du couple ( embedding, concept ) à extraire
        
        Retourne
        ----------
        ( list( float ), int )
            Le couple ( embedding, indice du concept associé ) associé à l'indice fournit en paramètre.
        '''
        return self.inputs[ index ], self.labels[ index ]

    
    def __repr__(self):
        return 'Dataset {{ "inputs": {}, "targets": {}, "bundle": {} }}'.format(
            self.inputs,
            self.labels,
            self.bundle
        )


    def __str__(self):
        return '--> Dataset\n----> {:<7}: {}\n----> {:<7}: {}\n----> {:<7}: {}\n'.format(
            'inputs',
            self.inputs,
            'targets',
            self.labels,
            'bundle',
            self.bundle,
        )
