#!/bin/bash
# ACHTUNG! Lancer ce script depuis le dossier bin/

DATA_DIR=../data
TSV_DIR=$DATA_DIR/tsv
BASENAME=dev.embed.layer-

mkdir -p ../data/tsv

for i in 1 2 3 4 5
do
    cut -d',' -f 3 $DATA_DIR/$BASENAME$i | sed 's/ /\t/g'  > $TSV_DIR/vec-$BASENAME$i.tsv
    echo -e "word\tmeaning" > $TSV_DIR/metadata-$BASENAME$i.tsv
    cut -d',' -f 1,2 $DATA_DIR/$BASENAME$i | sed 's/,/\t/g'  >> $TSV_DIR/metadata-$BASENAME$i.tsv
done